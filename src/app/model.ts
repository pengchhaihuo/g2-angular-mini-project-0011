export  interface Employee{
  email: String;
  firstname: String;
  lastname: String;
  gender: String;
  position: String;
}
