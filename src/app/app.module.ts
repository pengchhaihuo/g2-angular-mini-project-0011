import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {InputComponent} from './core/component/input/input.component';
import {ShowComponent} from './core/component/show/show.component';
import {MatSliderModule} from '@angular/material/slider';
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatTableModule} from "@angular/material/table";
import {MatButtonModule} from '@angular/material/button';
import {ViewDetailComponent} from './core/component/view-detail/view-detail.component';
import {MatDialogModule} from '@angular/material/dialog';
import {ViewUpdateComponent} from './core/component/view-update/view-update.component';
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatRadioModule} from "@angular/material/radio";
import {MatSelectModule} from "@angular/material/select";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { ObserveComponent } from './core/component/observe/observe.component';


// @ts-ignore
@NgModule({
  declarations: [
    AppComponent,
    InputComponent,
    ShowComponent,
    ViewDetailComponent,
    ViewUpdateComponent,
    ObserveComponent
  ],
    entryComponents:[ViewDetailComponent, ViewUpdateComponent],
  imports: [
    BrowserModule,
    MatSliderModule,
    MatSidenavModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    MatGridListModule,
    MatRadioModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatDialogModule,
    MatProgressBarModule,
    MatFormFieldModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
