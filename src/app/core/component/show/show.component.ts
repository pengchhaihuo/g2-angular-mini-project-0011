import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {ViewDetailComponent} from "../view-detail/view-detail.component";
import {ViewUpdateComponent} from "../view-update/view-update.component";
import {Employee} from "../../../model";



@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {


  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
    // console.log("show")
    // console.log("----------- table-------------")
    // console.log(this.employees);
  }

  @Input("emp")employees!: Employee[]


  openView(data: any) {
    const dialogRef = this.dialog.open(ViewDetailComponent, {
      data:{
        firstname: data.firstname,
        lastname: data.lastname,
        email: data.email,
        emp_type: data.position,
        gender: data.gender

      }
    });
  }

  onUpdate(emp:any,index:number) {
    this.getIndex.emit(index);
    this.empOut.emit(emp);
  }

  deleteData=(idx:any):void=>{

      this.employees.splice(idx, 1);

  }
  @Output() getIndex: EventEmitter<any> = new EventEmitter<any>()
  @Output() empOut : EventEmitter<any>  =new EventEmitter<any>()

}
