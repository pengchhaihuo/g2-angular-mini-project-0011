import {Component, OnInit} from '@angular/core';
// import {ErrorStateMatcher} from "@angular/material/core";
import {FormBuilder, FormGroup, FormGroupDirective, Validators} from "@angular/forms";
import {Employee} from "../../../model";

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})



export class InputComponent implements OnInit{



  //----------------------------------- form -----------------------------------------
  formInput! : FormGroup;
  positionInput:string[] = ['Admin','Developer','Designer','Tester']
  constructor(private formBuilder: FormBuilder) {
    this.formInput = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      firstname: ['', [Validators.required, Validators.maxLength(15), Validators.pattern("^[a-zA-Z]+$")]],
      lastname: ['', [Validators.required, Validators.maxLength(15), Validators.pattern("^[a-zA-Z]+$")]],
      gender: ['Male', [Validators.required]],
      position: ['', [Validators.required]]
    })
  }


  get firstname() {
    return this.formInput.get('firstname');
  }
  get lastname() {
    return this.formInput.get('lastname');
  }
  get email() {
    return this.formInput.get('email');
  }
  get gender() {
    return this.formInput.get('gender');
  }

  get position() {
    return this.formInput.get('position')
  }


  errorButton(): any {
    if (this.email?.invalid && this.email.touched || this.firstname?.invalid || this.lastname?.invalid || this.position?.invalid) {
      return 'true';
    } else {
      return 'false';
    }
  }

  indexForUpdate: number = -1;

  onSubmit(FormDirective: FormGroupDirective) {
    if(this.indexForUpdate == -1) {
      this.employees.push(this.formInput.value);
    }else{
      this.employees.splice(this.indexForUpdate,1,this.formInput.value);
    }
    FormDirective.resetForm();
    this.indexForUpdate =-1;
  }

  ngOnInit(): void {
    this.employees.push(this.emp1);
    this.employees.push(this.emp2);
    this.employees.push(this.emp3);
    this.employees.push(this.emp4);
    this.employees.push(this.emp5);
    this.employees.push(this.emp6);
    // console.log(this.employees)
  }

  employees: Employee[] = [];

  //---------------------- first initialize ---------------------------------
  emp1: Employee = {
    email: "pengchhaihuo@gmail.com",
    firstname: "Peng",
    lastname: "Chhaihuo",
    gender: "Male",
    position: "Developer"
  }
  emp2: Employee = {
    email: "porleas@gmail.com",
    firstname: "kung",
    lastname: "porleas",
    gender: "Male",
    position: "Designer"
  }
  emp3: Employee = {
    email: "sovannpich@gmail.com",
    firstname: "Theng",
    lastname: "Sovannpich",
    gender: "Male",
    position: "Tester"
  }
  emp4: Employee = {
    email: "timeng@gmail.com",
    firstname: "Lun",
    lastname: "Timeng",
    gender: "Male",
    position: "Developer"
  }
  emp5: Employee = {
    email: "theavoth@gmail.com",
    firstname: "Tum",
    lastname: "Sotheavoth",
    gender: "Male",
    position: "Developer"
  }
  emp6: Employee = {
    email: "boranjork@gmail.com",
    firstname: "Boran",
    lastname: "Jork",
    gender: "Male",
    position: "Tester"
  }


  onUpdate(data: any) {
    this.formInput.get('email'
    )?.setValue(data.email);
    this.formInput.get('firstname'
    )?.setValue(data.firstname);
    this.formInput.get('lastname'
    )?.setValue(data.lastname);
    this.formInput.get('gender'
    )?.setValue(data.gender);
    this.formInput.get('position')?.setValue(data.position);
  }

  getEmpIndexForUpdate(indexEmpUpdate: any) {
    this.indexForUpdate = indexEmpUpdate;
     console.log(this.indexForUpdate);
  }
}
